﻿using LS_API;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Runtime.InteropServices;

namespace EditLiquidHandling
{
    internal class Program
    {
        static List<double> parseValueList(string valueListString)
        {
            List<double> valueList = new List<double>();
            var parts = valueListString.Split(',');
            foreach (var part in parts)
            {
                valueList.Add(double.Parse(part));
            }

            return valueList;
        }

        static Point parsePoint(string value)
        {
            var point = new Point();
            var parts = value.Split(',');
            point.X = int.Parse(parts[0]);
            point.Y = int.Parse(parts[1]);

            return point;
        }
        
        static int Main(string[] args)
        {
            // from subprocess import run
            // run(['LSAPI__.exe', '100', '3', ','.join(values), ....])

            // Args:
            // design ID (int) e.g. 100
            // layerIndex (int) e.g. 3
            // value list (comma separated) e.g. 0,50,100,150,200,250,300,350,400,450,500,550
            // destStartCell (comma separated) e.g. 1,1
            // destEndCell (comma separated) e.g. 1,12

            // Notes for arguments:
            // layerIndex = 3 ; recipe 3
            // layerIndex = 4 ; recipe 4

            if (args.Length < 1)
            {
                Console.WriteLine("Design ID required");
                return 1;
            }
            else if (args.Length < 2)
            {
                Console.WriteLine("Value layerIndex (int) required");
                return 1;
            }
            else if (args.Length < 2)
            {
                Console.WriteLine("Value list (comma separated) required");
                return 1;
            }
            else if (args.Length < 3)
            {
                Console.WriteLine("destStartCell required (comma separated)");
                return 1;
            }
            else if (args.Length < 4)
            {
                Console.WriteLine("destEndCell required (comma separated)");
                return 1;
            }

            var designId = int.Parse(args[0].Split(',')[0]);
            var layerIndex = int.Parse(args[1].Split(',')[0]);
            var valueList = parseValueList(args[2]);
            var destStartCell = parsePoint(args[3]);
            var destEndCell = parsePoint(args[4]);
            var changesource = int.Parse(args[5]);

            LibraryStudioWrapper.GetDesignFromDatabase(designId, false);
            var recipe = LibraryStudioWrapper.GetMap(layerIndex, 1);

            // edit recipe 3, add discrete amounts 
            var mapType = recipe.Type;
            var sourceName = recipe.Name;
            var destName = recipe.Library;
            var selectedUnit = recipe.Unit;
            var tagList = recipe.TagList;

            var sourceStartCell = new Point(recipe.Values.Min(e => e.Item1), recipe.Values.Min(e => e.Item2));
            var sourceEndCell = new Point(recipe.Values.Max(e => e.Item1), recipe.Values.Max(e => e.Item2));
            Console.WriteLine(tagList);
            // temp fix for LSAPI spliting bug
            var tagListTempFix = tagList.Replace(",", "|");
            Console.WriteLine(tagListTempFix);
            if (changesource == 0)
            {
                // change both source and destination
                sourceStartCell = destStartCell;
                sourceEndCell = destEndCell;
            }
            else if (changesource == 1)
            {
                // only change destination
                int startRow = 0;
                int startColumn = 0;
                int endRow = 0;
                int endColumn = 0;
                bool isRow = true;
                // row and column values are in alternating positions, with first item starting with row
                foreach (var item in recipe.SourceArea as IEnumerable)
                {
                    if (isRow)
                    {
                        int newRow = 0;
                        if (int.TryParse(item.ToString(), out newRow))
                        {
                            startRow = (startRow <= 0) ? newRow : startRow;
                            endRow = (endRow <= 0) ? newRow : (endRow > 0 && endRow < newRow) ? newRow : endRow;
                        }
                        isRow = false;
                    }
                    else
                    {
                        int newCol = 0;
                        if (int.TryParse(item.ToString(), out newCol))
                        {
                            startColumn = (startColumn <= 0) ? newCol : startColumn;
                            endColumn = (endColumn <= 0) ? newCol : (endColumn > 0 && endColumn < newCol) ? newCol : endColumn;
                        }
                        isRow = true;
                    }
                }
                sourceStartCell.X = startRow;
                sourceStartCell.Y = startColumn;
                sourceEndCell.X = endRow;
                sourceEndCell.Y = endColumn;
            }
            else if (changesource == 2)
            {
                sourceStartCell = destStartCell;
                sourceEndCell = destEndCell;
                int startRow = 0;
                int startColumn = 0;
                int endRow = 0;
                int endColumn = 0;
                bool isRow = true;
                // row and column values are in alternating positions, with first item starting with row
                foreach (var item in recipe.DestinationArea as IEnumerable)
                {
                    if (isRow)
                    {
                        int newRow = 0;
                        if (int.TryParse(item.ToString(), out newRow))
                        {
                            startRow = (startRow <= 0) ? newRow : startRow;
                            endRow = (endRow <= 0) ? newRow : (endRow > 0 && endRow < newRow) ? newRow : endRow;
                        }
                        isRow = false;
                    }
                    else
                    {
                        int newCol = 0;
                        if (int.TryParse(item.ToString(), out newCol))
                        {
                            startColumn = (startColumn <= 0) ? newCol : startColumn;
                            endColumn = (endColumn <= 0) ? newCol : (endColumn > 0 && endColumn < newCol) ? newCol : endColumn;
                        }
                        isRow = true;
                    }
                }
                destStartCell.X = startRow;
                destStartCell.Y = startColumn;
                destEndCell.X = endRow;
                destEndCell.Y = endColumn;
            }
            
            var amount = valueList[0]; ;
            var EditIndex = 1;

            var result = LibraryStudioWrapper.EditArrayMap(sourceName, destName, mapType, selectedUnit, sourceStartCell,
                        sourceEndCell, destStartCell, destEndCell, amount, valueList, tagListTempFix, layerIndex, EditIndex);
            CheckError(result);
            Save();
            return 0;
        }

        static void Save()
        {
            var result = LibraryStudioWrapper.SaveDesignToDatabase(false, true);
            CheckError(result);
        }

        static void CheckError(int result)
        {
            if (result < 0)
                Console.WriteLine("Error: " + LibraryStudioWrapper.GetLastErrorMessage());
            else
                Console.WriteLine("Success");
        }
    }
}
