# unchained_labs_library_studio



## Getting started

Demo C# programs that use the Unchained Labs Library Studio API


## General Purpose Dosing Editing

### For liquid transfer from plate to plate:
**EditLiquidTransfer.cs**
path/to/EditLiquidTransfer.exe DESIGN_ID step_number volume dest_start_cell dest_end_cell source_destination_code 

source_destination_code:
```
SOURCE_AND_DEST = '0'   # change both source and destination
DESTINATION_ONLY = '1'  # destination only
SOURCE_ONLY = '2'       # source only
```

### For liquid transfer from source to plate:
**EditSolventHandling.cs**

path/to/EditSolventHandling.exe DESIGN_ID step_number volume dest_start_cell dest_end_cell
## License