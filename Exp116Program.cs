using LS_API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Symyx.SymyxDbUtilities.Interop;
using System.Security.Cryptography;

namespace LSAPI_exp116
{
    internal class Program
    {
        static List<double> parseValueList(string valueListString)
        {
            List<double> valueList = new List<double>();
            var parts = valueListString.Split(',');
            foreach (var part in parts)
            {
                valueList.Add(double.Parse(part));
            }

            return valueList;
        }

        static Point parsePoint(string value)
        {
            var point = new Point();
            var parts = value.Split(',');
            point.X = int.Parse(parts[0]);
            point.Y = int.Parse(parts[1]);

            return point;
        }

        static int Main(string[] args)
        {
            // from subprocess import run
            // run(['LSAPI__.exe', '100', '3', ','.join(values), ....])

            // Args:
            // design ID (int) e.g. 116
            // layerIndex (int) e.g. 1
            // value (int) e.g. 1000 (uL)
            // startCell (comma separated) e.g. 1,1
            // endCell (comma separated) e.g. 1,12

            // Notes for arguments:
            // layerIndex = 1 ; recipe 1
            // layerIndex = 2 ; recipe 2

            if (args.Length < 1)
            {
                Console.WriteLine("Design ID required");
                return 1;
            }
            else if (args.Length < 2)
            {
                Console.WriteLine("Value layerIndex (int) required");
                return 1;
            }
            else if (args.Length < 2)
            {
                Console.WriteLine("Value value (int) required");
                return 1;
            }
            else if (args.Length < 3)
            {
                Console.WriteLine("startCell required (comma separated)");
                return 1;
            }
            else if (args.Length < 4)
            {
                Console.WriteLine("endCell required (comma separated)");
                return 1;
            }

            var designId = int.Parse(args[0].Split(',')[0]);
            var layerIndex = int.Parse(args[1].Split(',')[0]);
            var valueForUniform = double.Parse(args[2].Split(',')[0]);
            var startCell = parsePoint(args[3]);
            var endCell = parsePoint(args[4]);

            LibraryStudioWrapper.GetDesignFromDatabase(designId, false);

            // edit recipe, add discrete amounts 
            var mapName = "water";
            var mapType = "Uniform";
            var selectedUnit = "ul";
            var library = "Plate1";
            var tagList = "H6Tip,Backsolvent";
            var EditIndex = 1;


            var positions = new List<Tuple<int, int>>();
            var discreteValues = new List<double>();

            for (int i = startCell.X; i <= endCell.X; i++)
            {
                for (int j = startCell.Y; j <= endCell.Y; j++)
                {
                    positions.Add(new Tuple<int, int>(i, j));
                    discreteValues.Add(valueForUniform);

                }

            }
             
            var result = LibraryStudioWrapper.EditSourceMap(mapName, mapType, selectedUnit, valueForUniform, positions, discreteValues, library, tagList, layerIndex, EditIndex);

            CheckError(result);
            Save();

            return 0;
        }

        static void Save()
        {
            var result = LibraryStudioWrapper.SaveDesignToDatabase(false, true);
            CheckError(result);
        }

        static void CheckError(int result)
        {
            if (result < 0)
                Console.WriteLine("Error: " + LibraryStudioWrapper.GetLastErrorMessage());
            else
                Console.WriteLine("Success");
        }
    }
}
