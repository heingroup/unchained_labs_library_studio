using LS_API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Symyx.SymyxDbUtilities.Interop;
using System.Security.Cryptography;

namespace LSAPI_exp100
{
    internal class Program
    {
        static List<double> parseValueList(string valueListString)
        {
            List<double> valueList = new List<double>();
            var parts = valueListString.Split(',');
            foreach (var part in parts)
            {
                valueList.Add(double.Parse(part));
            }

            return valueList;
        }

        static Point parsePoint(string value)
        {
            var point = new Point();
            var parts = value.Split(',');
            point.X = int.Parse(parts[0]);
            point.Y = int.Parse(parts[1]);

            return point;
        }

        static int Main(string[] args)
        {
            // from subprocess import run
            // run(['LSAPI__.exe', '100', '3', ','.join(values), ....])

            // Args:
            // design ID (int) e.g. 100
            // layerIndex (int) e.g. 3
            // value list (comma separated) e.g. 0,50,100,150,200,250,300,350,400,450,500,550
            // destStartCell (comma separated) e.g. 1,1
            // destEndCell (comma separated) e.g. 1,12

            // Notes for arguments:
            // layerIndex = 3 ; recipe 3
            // layerIndex = 4 ; recipe 4

            if (args.Length < 1)
            {
                Console.WriteLine("Design ID required");
                return 1;
            }
            else if (args.Length < 2)
            {
                Console.WriteLine("Value layerIndex (int) required");
                return 1;
            }
            else if (args.Length < 2) 
            {
                Console.WriteLine("Value list (comma separated) required");
                return 1;
            } else if (args.Length < 3)
            {
                Console.WriteLine("destStartCell required (comma separated)");
                return 1;
            } else if (args.Length < 4)
            {
                Console.WriteLine("destEndCell required (comma separated)");
                return 1;
            }

            var designId = int.Parse(args[0].Split(',')[0]);
            var layerIndex = int.Parse(args[1].Split(',')[0]);
            var valueList = parseValueList(args[2]);
            var destStartCell = parsePoint(args[3]);
            var destEndCell = parsePoint(args[4]);

            LibraryStudioWrapper.GetDesignFromDatabase(designId, false);

            // edit recipe 3, add discrete amounts 
            var sourceName = "Plate2";
            var destName = "Plate1";
            var maptype = "Discrete";
            var selectedUnit = "ul";
            var sourceStartcell = new Point(1, 1);
            var sourceEndcell = new Point(1, 6);
            var amount = 0;
            var tagList = "H6TIp";
            var EditIndex = 1;

            var result = LibraryStudioWrapper.EditArrayMap(sourceName, destName, maptype, selectedUnit, sourceStartcell,
                        sourceEndcell, destStartCell, destEndCell, amount, valueList, tagList, layerIndex, EditIndex);
            CheckError(result);
            Save();

            return 0;
        }

        static void Save()
        {
            var result = LibraryStudioWrapper.SaveDesignToDatabase(false, true);
            CheckError(result);
        }

        static void CheckError(int result)
        {
            if (result < 0)
                Console.WriteLine("Error: " + LibraryStudioWrapper.GetLastErrorMessage());
            else
                Console.WriteLine("Success");
        }
    }
}
